##
# Displays the current directory in the window title.
##

chpwd_functions=(${chpwd_functions[@]} "set_cwd_to_title")
precmd_functions=(${precmd_functions[@]} "set_cwd_to_title")

function set_cwd_to_title() {
  print -Pn "\e]2;%~\a"
}

