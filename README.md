# simplez, a minimal zsh configuration

simplez is a minimal set of dotfiles for [zsh][zsh] with a couple of
built-in components to simplify developing on multiple Maven and Java versions.

## What's included?

simplez bundles the following components.

* [jEnv][jenv] for managing multiple Java versions
* [mvnvm][mvnvm] for transparently managing multiple Maven versions
* [Node Version Manager][nvm] for managing multiple Node versions
* [zsh-git-prompt][zsh-git] for your Git prompt

## Installation

The easiest way is to make a clone of simplez and symlink it into `~/.zshrc`.

    % git clone --recursive git@bitbucket.org:luuuis/simplez.git ~/.simplez
    % ln -s ~/.simplez/zshrc ~/.zshrc

Alternatively, you can `source ~/.simplez/zshrc` from your own `.zshrc` and
add any additional customisations in there. If you are not using zsh yet,
 ow would be good time to change over. 

    % chsh -s `which zsh`

Any new shell that you open should now be using simplez. At this point
it's probably wise to test that everything works before closing the 
existing shell..

### jEnv configuration

You should now add any installed Java SDKs to jEnv. You will probably 
want to add a 1.6, 1.7, and 1.8 version. If you are on Mac OSX the best 
way to find the available Java versions is to use the `java_home` built-in.

    ~ % /usr/libexec/java_home -v 1.6
    /System/Library/Java/JavaVirtualMachines/1.6.0.jdk/Contents/Home
    ~ % jenv add /System/Library/Java/JavaVirtualMachines/1.6.0.jdk/Contents/Home
    oracle64-1.6.0.38 added
    1.6.0.38 added
    1.6 added
    
Feel free to add different flavours as well (OpenJDK, etc). You can choose
which one to use on a project later.

jEnv uses plugins to hook in to the invocation of other commands that are
built on Java. At the very least you will want to enable the Maven plugin.

    ~ % jenv enable-plugin maven
    maven plugin activated
    ~ % jenv enable-plugin scala maven
    scala plugin activated
    ~ % jenv plugins 
    ant
    export
    ...

## Usage

To use jEnv and mvnvm together on a project you need to tell each wrapper what version
to use. Each one has its own configuration file.

    % jenv local 1.6
    % echo "mvn_version=2.1.0" > mvnvm.properties
    % git add .java-version mvnvm.properties
    % git commit -m "configured jEnv and mvnvm for my project"
 
That's it!

    % mvn --version
    [MVNVM] Using maven: 2.1.0
    Apache Maven 2.1.0 (r755702; 2009-03-19 06:10:27+1100)
    Java version: 1.6.0_38
    Java home: /Library/Java/JavaVirtualMachines/1.6.0_38-b04-436.jdk/Contents/Home

Calling `jconsole` or other binaries directly will also use the right Java version. You
can also set specific Java options if these should be shared with other developers.

    % jenv local-options "-Xmx1024m -XX:MaxPermSize=256m"

These will be pushed into `MAVEN_OPTS` when running Maven if you have enabled the Maven 
plugin (also works for other tools that have a jEnv plugin).

If you want to get fancy with jEnv check `jenv help` to check out what else is 
available.


[zsh]: http://www.zsh.org/
[mvnvm]: https://bitbucket.org/mjensen/mvnvm
[nvm]: https://github.com/creationix/nvm
[jenv]: https://github.com/gcuisinier/jenv
[zsh-git]: https://github.com/olivierverdier/zsh-git-prompt

