##
# show the current git branch in prompt
##

source "$SIMPLEZ/zsh-git-prompt/zshrc.sh"
PROMPT='%B%1~%b$(git_super_status) %# '
