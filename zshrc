# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/Users/luis/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall
#

export SIMPLEZ=~/.simplez

# set up the basics
source "$SIMPLEZ"/alias.zsh
source "$SIMPLEZ"/history.zsh
source "$SIMPLEZ"/key-bindings.zsh
source "$SIMPLEZ"/prompt.zsh
source "$SIMPLEZ"/title.zsh

# dev environment (jenv last so it can shim everything else)
source "$SIMPLEZ"/mvnvm.zsh
source "$SIMPLEZ"/nvm.zsh
source "$SIMPLEZ"/sbt.zsh
source "$SIMPLEZ"/jenv.zsh

unset SIMPLEZ

